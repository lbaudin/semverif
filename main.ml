(*
  Cours "Sémantique et Application à la Vérification de programmes"
  
  Antoine Miné 2015
  Ecole normale supérieure, Paris, France / CNRS / INRIA
*)

(*
  Simple driver: parses the file given as argument and prints it back.

  You should modify this file to call your functions instead!
*)

open Value_domain
open Cfg
open Interval_domain
open Const_domain

module Iterator_int = Iterator.Iterator(IntervalDomain)
module Iterator_const = Iterator.Iterator(ConstDomain)

(* parse filename *)
let doit iterator filename =
  let prog = File_parser.parse_file filename in
  let cfg = Tree_to_cfg.prog prog in
  Printf.printf "%a" Cfg_printer.print_cfg cfg;
  Cfg_printer.output_dot "cfg.dot" cfg;
  iterator cfg;;


(* parses arguments to get filename *)
let main () =
  match Array.to_list Sys.argv with
  | _::domain::filename::_ -> begin
  	if domain = "const" then
  	doit Iterator_const.iterate_over_cfg filename
	else if domain = "interval" then
	doit Iterator_int.iterate_over_cfg filename
	else invalid_arg "the first argument must be the value domain: const or interval"
  end
  | _ -> invalid_arg "no source file or value domain specified (usage ./main.byte [const|interval] filename)"

let _ = main ()
