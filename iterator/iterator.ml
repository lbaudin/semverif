open Value_domain
open Cfg

module Iterator = functor (ValueDomain:VALUE_DOMAIN) ->
	struct

	module Domain = Domain_impl.Domain(ValueDomain)
	module DomMap = Map.Make(struct type t = int let compare = compare end)

	let create_dommap_from_nodes cfg =
		List.fold_left (fun dommap n ->
			DomMap.add n.node_id Domain.bottom dommap) DomMap.empty cfg.cfg_nodes

	let apply_instr cfg iterate_bwd dommap arc_in =
		let previous_domain = DomMap.find arc_in.arc_src.node_id dommap in
		let previous_domain_loop = DomMap.find arc_in.arc_dst.node_id dommap in
		if Domain.is_bottom previous_domain then
			Domain.bottom
		else
			match arc_in.arc_inst with

			| CFG_skip(s) -> previous_domain
			
			(* assignment *)
			| CFG_assign(v, expr) -> Domain.assign previous_domain v expr

			(* guard: test that must be satisfied to make a transition *)
			| CFG_guard(bexpr) -> Domain.guard previous_domain bexpr

			(* assertion: it is an error if the test is not satisfied *)
			| CFG_assert(bexpr) ->
				let guarded = Domain.guard previous_domain (Domain.neg_bool bexpr) in
				if Domain.is_bottom guarded then
					previous_domain
				else
					begin
					(* Start bwd analysis *)
					let empty_dommap = DomMap.add arc_in.arc_dst.node_id guarded dommap in
					let queue = Queue.create () in
					Queue.add arc_in.arc_src queue;
					let impossible = iterate_bwd queue empty_dommap in
					previous_domain
					end
				
			(* function call *)
			| CFG_call(func) -> DomMap.find func.func_exit.node_id dommap

			(* start of a loop or a backward goto, we can do a widening here *)
			| CFG_loop -> Domain.widen previous_domain_loop previous_domain
	
	let check_assertion cfg iterate_bwd dommap arc_in =
		let previous_domain = DomMap.find arc_in.arc_src.node_id dommap in
		match arc_in.arc_inst with

		(* assertion: it is an error if the test is not satisfied *)
		| CFG_assert(bexpr) ->
			let guarded = Domain.guard previous_domain (Domain.neg_bool bexpr) in
			begin
			(* Start bwd analysis *)
			let empty_dommap = DomMap.add arc_in.arc_dst.node_id guarded dommap in
			let queue = Queue.create () in
			Queue.add arc_in.arc_src queue;
			let impossible = iterate_bwd queue empty_dommap in
			if not impossible then
				Printf.printf "One assertion probably failed\n";
			end
			
		| _ -> ()
	

	let apply_instr_bwd dommap arc_in =
		let previous_domain = DomMap.find arc_in.arc_dst.node_id dommap in
		let previous_domain_loop = DomMap.find arc_in.arc_src.node_id dommap in
		if Domain.is_bottom previous_domain then
			Domain.bottom
		else
			match arc_in.arc_inst with

			| CFG_skip(s) -> previous_domain
			
			(* assignment *)
			| CFG_assign(v, expr) -> Domain.assign_bwd previous_domain_loop v expr previous_domain

			(* guard: test that must be satisfied to make a transition *)
			| CFG_guard(bexpr) -> Domain.meet previous_domain_loop (Domain.guard previous_domain bexpr)

			(* assertion: it is an error if the test is not satisfied *)
			| CFG_assert(bexpr) -> previous_domain
				
			(* function call, not implemented backward *)
			 | CFG_call(func) -> DomMap.find func.func_exit.node_id dommap

			(* start of a loop or a backward goto, we can do a widening here *)
			| CFG_loop -> Domain.widen previous_domain previous_domain_loop


	let put_function_in_queue q f =
		List.iter (fun a ->
			Queue.add a.arc_dst q;) f.func_entry.node_out

	let rec call_functions iterate_aux dommap = function
		| [] -> dommap
		| arc_in::q ->
			match arc_in.arc_inst with
			| CFG_call(func) ->
				begin
				let call_queue = Queue.create () in
				Queue.add func.func_entry call_queue;

				(* Start the function with the good domain *)
				let previous_domain = DomMap.find arc_in.arc_src.node_id dommap in

				let new_map = iterate_aux previous_domain call_queue dommap in
				call_functions iterate_aux new_map q
				end
			| _ -> call_functions iterate_aux dommap q
	and
	iterate_aux cfg (init_domain:Domain.t) (worklist:node Queue.t) (dommap: Domain.t DomMap.t) =
		try
			begin
			let n = Queue.take worklist in

			let previous_domain = DomMap.find n.node_id dommap in

			let arcs_in = n.node_in in

			(* Call functions if needed *)
			let dommap = call_functions (iterate_aux cfg) dommap arcs_in in

			(* Compute the domains after the instruction from every arc. *)
			let domains_after_instr = List.map (apply_instr cfg (iterate_bwd cfg init_domain) dommap) arcs_in in

			(* Join them *)
			let new_domain = if domains_after_instr = [] then init_domain
				else List.fold_left Domain.join Domain.bottom domains_after_instr in

			(* Maybe subset is sufficient here, but I let equal to avoid potential bugs in the extensions. *)
			if not(Domain.equal previous_domain new_domain) then
				(* Add the successor of this node to the worklist *)
				List.iter (fun arc_out ->
					Queue.add arc_out.arc_dst worklist) n.node_out;
			iterate_aux cfg init_domain worklist (DomMap.add n.node_id new_domain dommap)
			end
		with
			| Queue.Empty -> dommap
	and
	iterate_bwd cfg (init_domain:Domain.t) (worklist:node Queue.t) (dommap: Domain.t DomMap.t) =
		try
			begin
			let n = Queue.take worklist in

			let previous_domain = DomMap.find n.node_id dommap in

			let arcs_out = n.node_out in

			(* Call functions if needed, not implemented backward *)
			(* let dommap = call_functions dommap arcs_out in *)

			(* Compute the domains after the instruction from every arc. *)
			let domains_after_instr = List.map (apply_instr_bwd dommap) arcs_out in

			(* Join them *)
			let new_domain = if domains_after_instr = [] then init_domain
				else List.fold_left Domain.join Domain.bottom domains_after_instr in

			(* Maybe subset is sufficient here, but I let equal to avoid potential bugs in the extensions. *)
			if not(Domain.equal previous_domain new_domain) then
				(* Add the successor of this node to the worklist *)
				List.iter (fun arc_in ->
					Queue.add arc_in.arc_src worklist) n.node_in;
			iterate_bwd cfg init_domain worklist (DomMap.add n.node_id new_domain dommap)
			end
		with
			| Queue.Empty ->
			begin
			let map = DomMap.find cfg.cfg_init_entry.node_id dommap in
			Domain.is_bottom map
			end


	let iterate_over_cfg cfg =
		let init_domain = Domain.init cfg.cfg_vars in

		let empty_dommap = create_dommap_from_nodes cfg in
		let worklist = Queue.create () in
		Queue.add cfg.cfg_init_entry worklist;

		let domains_initialized = iterate_aux cfg init_domain worklist empty_dommap in
		try
		begin
			let main = List.find (fun f -> f.func_name = "main") cfg.cfg_funcs in
			(* Context *)
			let init_domain = DomMap.find cfg.cfg_init_exit.node_id domains_initialized in
			Queue.add main.func_entry worklist;
			
			let cfg = { cfg with cfg_init_entry = main.func_entry; } in
			let final_domains = iterate_aux cfg init_domain worklist domains_initialized in
			Printf.printf "Result of the analysis:\n======================\n";
			Domain.print stdout (DomMap.find main.func_exit.node_id final_domains);
			
			List.iter (check_assertion cfg (iterate_bwd cfg init_domain) final_domains) cfg.cfg_arcs;
		end
		with | Not_found -> Printf.printf "A main function is necessary."
	
	end


