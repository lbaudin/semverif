open Domain
open Const_domain
open Interval_domain
open Cfg
open Abstract_syntax_tree
open Value_domain

module Domain : DOMAIN = functor (ValueDomain:VALUE_DOMAIN) ->
	struct

	module VarMap = Map.Make(Var)

	type t  = ValueDomain.t VarMap.t
	
	let bottom = VarMap.empty
	
	let print out_channel dom =
		VarMap.iter (fun v v_val ->
			Printf.fprintf out_channel "%s = \n" v.var_name;
			ValueDomain.print out_channel v_val) dom


	let init = List.fold_left (fun dom v ->
		VarMap.add v (ValueDomain.const Z.zero) dom) bottom
	
	let is_bottom d = VarMap.is_empty d
	
	let strict f = fun dom ->
		if is_bottom dom then dom
		else f dom


	let rec get_value dom = function
		| CFG_int_const(i) -> ValueDomain.const i

		(* unary operation *)
		| CFG_int_unary(uop, expr) ->
			let value = get_value dom expr in
			ValueDomain.unary value uop


		(* binary operation *)
		| CFG_int_binary (bop, e1, e2) ->
			let v1 = get_value dom e1 and v2 = get_value dom e2 in
			ValueDomain.binary v1 v2 bop

		(* variable use *)
		| CFG_int_var(v') -> VarMap.find v' dom

		(* non-deterministic choice between two integers *)
		| CFG_int_rand(a,b) -> ValueDomain.rand a b
    

	let assign = fun dom v expr ->
		let value = get_value dom expr in
		VarMap.add v value dom
	
	let rec bwd dom expr (val_wanted:ValueDomain.t) = match expr with
		| CFG_int_const(a) -> if ValueDomain.subset (ValueDomain.const a) val_wanted then dom else bottom
		| CFG_int_rand(a, b) ->
			if ValueDomain.is_bottom (ValueDomain.meet val_wanted (ValueDomain.rand a b)) then
				bottom
			else
				dom
		| CFG_int_unary(uop, expr) ->
			let dom' = bwd dom expr (ValueDomain.bwd_unary (get_value dom expr) uop val_wanted )
			in dom'
		| CFG_int_binary(bop, e1, e2) ->
			let val_wanted1, val_wanted2 = ValueDomain.bwd_binary (get_value dom e1) (get_value dom e2) bop val_wanted in
			let dom' = bwd dom e1 val_wanted1 in
			let dom' = bwd dom' e2 val_wanted2 in
			dom'
		| CFG_int_var(v') ->
			let current_val = VarMap.find v' dom in
			let new_val = (ValueDomain.meet current_val val_wanted) in
			if ValueDomain.is_bottom new_val then
				bottom
			else
				VarMap.add v' new_val dom
	
	let neg_op = function
		| AST_EQUAL         (* e == e *) -> AST_NOT_EQUAL
		| AST_NOT_EQUAL     (* e != e *) -> AST_EQUAL
		| AST_LESS          (* e < e *) -> AST_GREATER_EQUAL
		| AST_LESS_EQUAL    (* e <= e *) -> AST_GREATER
		| AST_GREATER       (* e > e *) -> AST_LESS_EQUAL
		| AST_GREATER_EQUAL (* e >= e *) -> AST_LESS
	
	let rec neg_bool = function
		| CFG_compare(op, e1, e2) -> CFG_compare(neg_op op, e1, e2)
		| CFG_bool_unary(AST_NOT, b) -> b
		| CFG_bool_rand -> CFG_bool_rand
		| CFG_bool_const(b) -> CFG_bool_const(not b)
		| CFG_bool_binary(op, e1, e2) -> match op with
			| AST_AND -> CFG_bool_binary(AST_OR, neg_bool e1, neg_bool e2)
			| AST_OR -> CFG_bool_binary(AST_AND, neg_bool e1, neg_bool e2)

	let join dom1 dom2 =
		VarMap.fold (fun id val_dom1 dom ->
			try
				let val_dom2 = VarMap.find id dom2 in
				VarMap.add id (ValueDomain.join val_dom1 val_dom2) dom
			with
				Not_found -> VarMap.add id val_dom1 dom
				) dom1 dom2
	
	let meet dom1 dom2 =
		if is_bottom dom1 then bottom else
		VarMap.fold (fun id val_dom1 dom ->
			try
				if is_bottom dom then bottom
				else
					let val_dom2 = VarMap.find id dom2 in
					let meeting = ValueDomain.meet val_dom1 val_dom2 in
					if ValueDomain.is_bottom meeting then bottom
					else VarMap.add id meeting dom
			with
				Not_found -> bottom
				) dom1 dom2

	let rec guard dom bexpr = match bexpr with
		| CFG_compare(op, e1, e2) ->
			begin
			let val_right = get_value dom e2 in
			let val_left = get_value dom e1 in

			let val_left_wanted, val_right_wanted = ValueDomain.compare val_left val_right op in

			let dom = bwd dom e1 val_left_wanted in
			let dom = bwd dom e2 val_right_wanted in
			dom
			end


		| CFG_bool_unary(AST_NOT, b) -> guard dom (neg_bool b)
		| CFG_bool_binary(AST_AND, b1, b2) -> begin
			meet (guard dom b1) (guard dom b2) end
		| CFG_bool_binary(AST_OR, b1, b2) -> join (guard dom b1) (guard dom b2)
	
	let subset a b = VarMap.fold (fun va value is_subset ->
		if VarMap.mem va b then
			(ValueDomain.subset value (VarMap.find va b)) && is_subset
		else
			false) a true


	let widen a b =
		let force_widen ab =
			VarMap.mapi (fun k _ ->
				let oldv = if VarMap.mem k a then VarMap.find k a else ValueDomain.bottom in
				let newv = ValueDomain.widen oldv (VarMap.find k b) in
				newv) ab
		in
		if subset a b then
			force_widen (join a b)
		else a
		
	let assign_bwd dom v expr after =
		if is_bottom after then bottom
		else bwd dom expr (VarMap.find v after)

	let equal = VarMap.equal ValueDomain.equal
	end
