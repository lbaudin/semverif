open Value_domain
open Abstract_syntax_tree

module IntervalDomain : VALUE_DOMAIN =
	struct
	type n = Ent of Z.t | Minf | Pinf
	type t = | TOP | BOTTOM | Int of n * n

	let nneg = function
	| Ent(z) -> Ent(Z.neg z)
	| Minf -> Pinf
	| Pinf -> Minf

	let naddsup (a:n) = function
	| Pinf -> Pinf
	| Minf -> if a = Pinf then Pinf else Minf
	| Ent(a') -> match a with
		| Pinf | Minf -> a
		| Ent(b) -> Ent(Z.add a' b)
	
	let naddinf a = function
	| Minf -> Minf
	| Pinf -> if a = Minf then Minf else Pinf
	| Ent(a') -> match a with
		| Pinf | Minf -> a
		| Ent(b) -> Ent(Z.add a' b)

	let nsubinf a b = naddinf a (nneg b)
	let nsubsup a b = naddsup a (nneg b)

	let xchg (a, b) = (b, a)

	let rec nmul a = function
	| Pinf ->
		begin
		match a with
		| Ent(a') -> if Z.gt a' Z.zero then Pinf
			else if Z.lt a' Z.zero then Minf
			else Ent(Z.zero)
		| _ -> a
		end
	| Minf -> nneg (nmul a Pinf)
	| Ent(b') -> match a with
		| Ent(c') -> Ent(Z.mul b' c')
		| Pinf | Minf -> nmul (Ent(b')) a
	
	let rec ndiv a = function
	| Pinf ->
		begin
		match a with
		| Ent(a') -> if Z.gt a' Z.zero then Pinf
			else if Z.lt a' Z.zero then Minf
			else Ent(Z.zero)
		| _ -> a
		end
	| Minf -> nneg (ndiv a Pinf)
	| Ent(b') -> match a with
		| Ent(c') -> Ent(Z.div b' c')
		| Pinf | Minf -> ndiv (Ent(b')) a
	
	
	let nmin a = function
	| Pinf -> a
	| Minf -> Minf
	| Ent(c) -> match a with
		| Pinf -> Ent(c)
		| Minf -> Minf
		| Ent(d) -> Ent(Z.min c d)
	
	let nmax a b = nneg (nmin (nneg a) (nneg b))

	let ngeq a = function
	| Minf -> true
	| Pinf -> a = Pinf
	| Ent(c) -> match a with
		| Minf -> false
		| Pinf -> true
		| Ent(d) -> Z.geq d c
	
	let nleq a b = ngeq (nneg a) (nneg b)

	let nlt a b = nleq a b && a <> b
	let ngt a b = ngeq a b && a <> b

	let nto_string = function
	| Pinf -> "pinf"
	| Minf -> "minf"
	| Ent(c) -> Z.to_string c
	
	let top = TOP
	let bottom = BOTTOM

	let const a = Int(Ent(a), Ent(a))
	let rand a b = Int(Ent(a), Ent(b))
	let unary a = function
	| AST_UNARY_PLUS -> a
	| AST_UNARY_MINUS -> match a with
		| TOP | BOTTOM -> a
		| Int(c, d) -> Int(nneg d, nneg c)
	
	let meet a b = match a, b with
		| BOTTOM, _ -> BOTTOM
		| _, BOTTOM -> BOTTOM
		| TOP, _ -> b
		| _, TOP -> a
		| Int(c, d), Int(c', d') ->
			let lub = (nmax c c') and gub = (nmin d d') in
			if nleq lub gub then Int(lub, gub) else BOTTOM
	

	let rec binary a b op = match a, b, op with
	| BOTTOM, _, _ -> BOTTOM
	| _, BOTTOM , _-> BOTTOM
	| TOP, _, _ -> TOP
	| _, TOP, _ -> binary b a op
	| Int(c, d), Int(c', d'), op ->
		match op with
		| AST_PLUS          (* e + e *) -> Int(naddinf c c', naddsup d d')
		| AST_MINUS         (* e - e *) -> Int(nsubinf c d', nsubsup d c')
		| AST_MULTIPLY      (* e * e *) ->
			let p1 = nmul c c' in
			let p2 = nmul c d' in
			let p3 = nmul d c' in
			let p4 = nmul d d' in
			Int(nmin (nmin (nmin p1 p2) p3) p4, nmax (nmax (nmax p1 p2) p3 ) p4)
		| AST_DIVIDE      (* e * e *) ->
			let p1 = ndiv c c' in
			let p2 = ndiv c d' in
			let p3 = ndiv d c' in
			let p4 = ndiv d d' in
			Int(nmin (nmin (nmin p1 p2) p3) p4, nmax (nmax (nmax p1 p2) p3 ) p4)
		| AST_MODULO -> meet a b

	let print out_channel = function
		| TOP -> Printf.fprintf out_channel "%s\n" "top"
		| BOTTOM -> Printf.fprintf out_channel "%s\n" "bottom"
		| Int(i, j) -> Printf.fprintf out_channel "[%s; %s]\n" (nto_string i) (nto_string j)

	let is_bottom a = a = BOTTOM
	let subset a b =
		match b with
		| TOP -> true
		| BOTTOM -> a = BOTTOM
		| Int(c, d) ->
			match a with
			| TOP -> false
			| BOTTOM -> true
			| Int(c', d') -> ngeq c' c && nleq d' d

	let widen a b = if subset a b then
		match a, b with
		| TOP, _ -> TOP
		| BOTTOM, _ -> b
		| _, TOP -> TOP
		| _, BOTTOM -> BOTTOM
		| Int(c', d'), Int(c, d) ->
			begin
			if nlt c (Ent(Z.of_int (-100))) && nlt c c' then
				Int(Minf, d)
			else if ngt d (Ent(Z.of_int 100)) && nlt d d' then
				Int(c, Pinf)
			else
				Int(c, d)
			end
		else
			a

	let join a b = match a, b with
		| TOP, _ -> TOP
		| _, TOP -> TOP
		| BOTTOM, _ -> b
		| _, BOTTOM -> a
		| Int(c, d), Int(c', d') -> Int(nmin c c', nmax d d')

	let rec compare a b = function
		| AST_EQUAL         (* e == e *) -> (meet a b, meet a b)
		| AST_NOT_EQUAL     (* e != e *) ->
		begin
			match a,b with
			| TOP, _ -> (TOP, b)
			| _, TOP -> (a, TOP)
			| BOTTOM, _ | _, BOTTOM -> (BOTTOM, BOTTOM)
			| Int(c, d), Int(c', d') -> if c = d && c' = d' && c = c' then (BOTTOM, BOTTOM)
				else (a,b)
		end
		| AST_LESS          (* e < e *) ->
		begin
			match a, b with
			| TOP, TOP -> (TOP, TOP)
			| BOTTOM, _ | _, BOTTOM -> (BOTTOM, BOTTOM)
			| Int(c, d), TOP -> (Int(c, d), Int(c, Pinf))
			| TOP, Int(c, d) -> (Int(c, d), Int(c, d))
			| Int(c, d), Int(c', d') ->
				if ngt c d' then (BOTTOM, BOTTOM)
				else (Int(c, nmin d (nsubinf d' (Ent(Z.one)))), Int(nmax (naddsup c (Ent(Z.one))) c', d'))
		end
		| AST_LESS_EQUAL    (* e <= e *)  ->
		begin
			match a, b with
			| TOP, TOP -> (TOP, TOP)
			| BOTTOM, _ | _, BOTTOM -> (BOTTOM, BOTTOM)
			| Int(c, d), TOP -> (Int(c, d), Int(c, Pinf))
			| TOP, Int(c, d) -> (Int(c, d), Int(c, d))
			| Int(c, d), Int(c', d') ->
				if ngt c d' then (BOTTOM, BOTTOM)
				else (Int(c, nmin d d'), Int(nmax c c', d'))
		end
		| AST_GREATER       (* e > e *) -> xchg (compare b a AST_LESS)
		| AST_GREATER_EQUAL (* e >= e *) -> xchg (compare b a AST_LESS_EQUAL)

	(* we filter x and we know that op x = r, donc x = op^-1 r *)
	let bwd_unary x op r =
		match op with
		| AST_UNARY_PLUS -> meet x (unary r AST_UNARY_MINUS)
		| AST_UNARY_MINUS -> meet x (unary r AST_UNARY_PLUS)

	(* we know that x op y = r, so x = r op^-1 y, y = r op^-1 x *)
	let bwd_binary x y op r =
		match op with
		| AST_PLUS          (* e + e *) -> (meet x (binary r y  AST_MINUS), meet y (binary r x AST_MINUS))
		| AST_MINUS         (* e - e *) -> (meet x (binary r y  AST_PLUS), meet y (binary x r AST_MINUS))
		| AST_MULTIPLY      (* e * e *) -> (meet x (binary r y  AST_DIVIDE), meet y (binary r y AST_DIVIDE))
		| AST_DIVIDE        (* e / e *) -> (meet x (binary r y  AST_MINUS), meet y (binary x r AST_DIVIDE))
		| AST_MODULO        (* e / e *) -> (meet x TOP, meet y TOP)

	let equal a b = a = b
end
