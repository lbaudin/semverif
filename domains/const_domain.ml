open Value_domain
open Abstract_syntax_tree

module ConstDomain : VALUE_DOMAIN =
	struct
	type t = | TOP | BOTTOM | INT of (Z.t)

	let top = TOP
	let bottom = BOTTOM

	let const a = INT(a)
	let rand a b = TOP
	let unary a = function
	| AST_UNARY_PLUS -> a
	| AST_UNARY_MINUS -> match a with
		| TOP | BOTTOM -> a
		| INT(c) -> INT(Z.neg c)

	let rec binary a b op = match a, b, op with
	| BOTTOM, _, _ -> BOTTOM
	| _, BOTTOM , _-> BOTTOM
	| TOP,INT(a),AST_MULTIPLY when a = Z.zero -> INT(Z.zero)
	| TOP, _, _ -> TOP
	| _, TOP, _ -> binary b a op
	| INT(a'), INT(b'), op ->
		match op with
		| AST_PLUS          (* e + e *) -> INT(Z.add a' b')
		| AST_MINUS         (* e - e *) -> INT(Z.sub a' b')
		| AST_MULTIPLY      (* e * e *) -> INT(Z.mul a' b')
		| AST_DIVIDE        (* e / e *) -> if b' = Z.zero then BOTTOM else INT(Z.div a' b')
		| AST_MODULO        (* e / e *) -> if b' = Z.zero then BOTTOM else INT(Z.rem a' b')

	let print out_channel = function
		| TOP -> Printf.fprintf out_channel "%s\n" "top"
		| BOTTOM -> Printf.fprintf out_channel "%s\n" "bottom"
		| INT(i) -> Printf.fprintf out_channel "%s\n" (Z.to_string i)

	let is_bottom a = a = BOTTOM

	let subset a b =
		match a with
		| TOP -> TOP = b
		| BOTTOM -> true
		| INT(a') when a = b || b = TOP -> true
		| _ -> false

	let widen a b = b
	
	let xchg (a, b) = (b, a)

	let meet a b = match a, b with
		| BOTTOM, _ -> BOTTOM
		| TOP, _ -> b
		| _, TOP -> a
		| INT(a'), _ -> if a = b then a else BOTTOM
	
	let join a b = match a, b with
		| TOP, _ -> TOP
		| _, TOP -> TOP
		| BOTTOM, _ -> b
		| _, BOTTOM -> a
		| INT(a'), INT(b') -> if a' = b' then INT(a') else TOP

	let rec compare a b = function
		| AST_EQUAL         (* e == e *) -> if subset a b then (a, a)
			else if subset b a then (b, b)
			else (BOTTOM, BOTTOM)
		| AST_NOT_EQUAL     (* e != e *) ->
		begin
			match a,b with
			| TOP, TOP -> (TOP, TOP)
			| BOTTOM, _ | _, BOTTOM -> (BOTTOM, BOTTOM)
			| TOP, INT(a) -> (TOP, INT(a))
			| INT(a), TOP -> (INT(a), TOP)
			| INT(a), INT(b) -> if a = b then (BOTTOM, BOTTOM) else (INT(a), INT(b))
		end
		| AST_LESS          (* e < e *) ->
			begin
			match a with
			| TOP -> (TOP, b)
			| BOTTOM -> (BOTTOM, BOTTOM)
			| INT(a') -> match b with
				| TOP -> (a, TOP)
				| BOTTOM -> (BOTTOM, BOTTOM)
				| INT(b') -> if Z.lt a' b' then (a, b) else (BOTTOM, BOTTOM)
			end
		| AST_LESS_EQUAL    (* e <= e *) ->
			begin
			match a with
			| TOP -> (TOP, b)
			| BOTTOM -> (BOTTOM, BOTTOM)
			| INT(a') -> match b with
				| TOP -> (a, TOP)
				| BOTTOM -> (BOTTOM, BOTTOM)
				| INT(b') -> if Z.leq a' b' then (a, b) else (BOTTOM, BOTTOM)
			end
		| AST_GREATER       (* e > e *) -> xchg (compare b a AST_LESS)
		| AST_GREATER_EQUAL (* e >= e *) -> xchg (compare b a AST_LESS_EQUAL)

	(* we filter x and we know that op x = r, donc x = op^-1 r *)
	let bwd_unary x op r =
		match op with
		| AST_UNARY_PLUS -> meet x (unary r AST_UNARY_MINUS)
		| AST_UNARY_MINUS -> meet x (unary r AST_UNARY_PLUS)

	(* we know that x op y = r, so x = r op^-1 y, y = r op^-1 x *)
	let bwd_binary x y op r =
		match op with
		| AST_PLUS          (* e + e *) -> (meet x (binary r y  AST_MINUS), meet y (binary r x AST_MINUS))
		| AST_MINUS         (* e - e *) -> (meet x (binary r y  AST_PLUS), meet y (binary x r AST_MINUS))
		| AST_MULTIPLY      (* e * e *) -> (meet x (binary r y  AST_DIVIDE), meet y (binary r y AST_DIVIDE))
		| AST_DIVIDE        (* e / e *) -> (meet x (binary r y  AST_MINUS), meet y (binary x r AST_DIVIDE))
		| AST_MODULO        (* e / e *) -> (meet x TOP, meet y TOP)

	let equal a b = a = b
end
