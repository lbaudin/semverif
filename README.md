Projet d'analyseur statique
---------------------------

# Domaine des valeurs

On peut changer le domaine des valeurs au lancement de l'analyse : le premier
argument est le nom de celui-ci (`interval` ou `const`).

Un foncteur de l'itérateur puis des environement permet de faire ce changement.

# Itérateur

## Widening

Le widening est implémenté assez simplement : si, sur une boucle, l'environement
précédent est inclus strictement dans le nouveau, et que des valeurs excèdent
100, alors on les met à plus l'infini.

## Assertions

J'avais au départ implémenté la vérification d'assertion comme un `if`, ce qui
posait problème dans des cas comme :
```
x = rand(0,10);
assert(x > 3);
```
où l'assetion pouvait, ou ne pouvait pas être réalisée.

C'est donc finalement implémenté comme un `else` (avec une négation de
l'assertion), ce qui peut éventuellement faire de faux-positifs.


L'analyse se fait en deux temps : d'abord on applique la stratégie de la
`worklist` suggérée dans l'énoncé, puis on reparcourt toutes les arêtes du CFG,
pour vérifier les assertions, et afficher un message d'erreur si nécessaire.

# Extensions

## Appels de fonctions

J'ai implémenté les appels de fonctions. Plutôt que d'ajouter des arêtes dans le
graphe de flots de controle, j'ai décidé de créer des nouveaux contextes et une
nouvelle file pour l'itérateur. Je pense que cela rend l'implémentation des
fonctions récursives plus faciles, puisque la pile est ensuite assez facile à
implémenter (mais je ne l'ai pas fait par manque de
temps).

Lorsqu'il y a un appel de fonction, l'environement de l'appelant est copié sur
le nœud d'initialisation de la fonction. Puis, on crée une nouvelle file, et on
relance l'itérateur en partant de ce nœud d'initialisation.

## Analyse en arrière

L'analyse en arrière reprend essentiellement le code de l'analyse en avant.
L'appel de fonctions n'est pas supporté, mes choix d'implémentation de
l'extension précédente n'étant pas forcément très adapté à l'analyse en arrière.

