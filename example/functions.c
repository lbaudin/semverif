int a1;

int a2 = 0;

int a3 = 4;

int f2() {
	a2 = a2+1;
}

int f1() {
	a1 = 1;
	f2();
}

int f3() {
	a3 = a3 - 1;
}

int fun_with_args(int c, int d) {
	return c - d;
}


int main() {
	f1();

	f2();

	f3();

	int r = fun_with_args(14, 35);
}
