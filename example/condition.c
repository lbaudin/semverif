int main() {
	int a = 4;
	int b = 5;

	if(a == 4 || b > 9) {
		b = 7;
	}

	assert(b==7);
}
