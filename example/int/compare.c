int main() {
	int a = 3;
	int a1;

	int a2 = 3;

	if(a == 3) {
		a1 = 1;
	}
	else {
		a1 = 2;
	}

	if(a == 4) {
		a2 = 5;
	}
	else {
		a2 = 2;
	}

	int a3 = 0;

	if(a != 3) {
		a3 = 1;
	}
	else {
		a3 = 3;
	}
	
	int a4 = 0;

	if(a != 5) {
		a4 = 4;
	}
	else {
		a4 = 3;
	}

	a = rand(1,5);

	int a5;
	if(a > 6) {
		a5 = 0;
	}
	else {
		a5 = 5;
	}


	int a6 = 0;

	if(a > 2) {
		if(a >= 3) {
			a6 = 6;
		}
	}
	else {
		if(a <= 2) {
			a6 = 6;
		}
	}
	
	assert(a1 == 1);
	assert(a2 == 2);
	assert(a3 == 3);
	assert(a4 == 4);
	assert(a5 == 5);
	assert(a6 == 6);

}
